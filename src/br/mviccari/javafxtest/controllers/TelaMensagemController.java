package br.mviccari.javafxtest.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;

/**
 * Created by Avell G1711 NEW on 17/05/2014.
 */
public class TelaMensagemController {

    @FXML
    private TextArea campoTexto;

    @FXML
    private AnchorPane root;

    private String parametroDeTeste;

    public void init() {
        try {
            campoTexto.setText(campoTexto.getText()+" - Parametro passado: "+parametroDeTeste);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void acaoOk(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        stage.close();
    }

    public void acaoFodase(ActionEvent actionEvent) {
        JOptionPane.showMessageDialog(null,"Vou contar tudo pra sua mãe! Parametro: "+parametroDeTeste);
        Stage stage = (Stage) root.getScene().getWindow();
        stage.close();
    }

    public String getParametroDeTeste() {
        return parametroDeTeste;
    }

    public void setParametroDeTeste(String parametroDeTeste) {
        this.parametroDeTeste = parametroDeTeste;
    }
}
