package br.mviccari.javafxtest.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.dialog.Dialogs;

import javax.swing.*;

/**
 * Created by Avell G1711 NEW on 17/05/2014.
 */
public class ExemploController {
    @FXML
    private Label labelStatus;

    public void newIssueFired(ActionEvent actionEvent) {
        labelStatus.setText("Você clicou em NOVO!");
    }

    public void saveIssueFired(ActionEvent actionEvent) {
        labelStatus.setText("Você clicou em SALVAR!");
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/br/mviccari/javafxtest/layouts/telaMensagem.fxml"));
            Parent root = fxmlLoader.load();
            TelaMensagemController controller = fxmlLoader.getController();
            controller.setParametroDeTeste("PIKACHU EU ESCOLHO VC!");
            controller.init();
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(labelStatus.getScene().getWindow());
            stage.setOpacity(1);
            stage.setTitle("My New Stage Title");
            stage.setScene(new Scene(root, 450, 450));
            stage.showAndWait();
            JOptionPane.showMessageDialog(null,"Fechou!");
        }catch (Exception ex){

        }
    }

    public void deleteIssueFired(ActionEvent actionEvent) {
        String s = JOptionPane.showInputDialog("Digite o q vc quer FDP:");
        labelStatus.setText("Você clicou em EXCLUIR! E vc digitou "+s+"!");
        Dialogs dialogs = Dialogs.create();
        dialogs.message("Olá!");
    }
}
